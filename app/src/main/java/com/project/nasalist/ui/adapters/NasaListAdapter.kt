package com.project.nasalist.ui.adapters

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.project.nasalist.R
import com.project.nasalist.model.PhotoInfo
import com.squareup.picasso.Picasso
import jp.wasabeef.picasso.transformations.RoundedCornersTransformation
import java.lang.ref.WeakReference


class NasaListAdapter(private val callbackWeakRef: WeakReference<NasaListAdapter.NasaItemInterface>) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private val photoItems = mutableListOf<PhotoInfo>()

    interface NasaItemInterface {
        fun onNasaItemClicked(image: String?)

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return PhotoItemViewHolder(parent)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        (holder as PhotoItemViewHolder).onBind(photoItems[position], onClick = {
            callbackWeakRef.get()?.onNasaItemClicked(it)
        })
    }

    override fun getItemCount(): Int {
        return photoItems.size
    }

    @SuppressLint("NotifyDataSetChanged")
    fun setItems(photoItems: List<PhotoInfo>) {
        this.photoItems.clear()
        this.photoItems.addAll(photoItems)
        notifyDataSetChanged()
    }

    inner class PhotoItemViewHolder(parent: ViewGroup) : RecyclerView.ViewHolder(
        LayoutInflater.from(parent.context).inflate(R.layout.photo_item, parent, false)
    ) {

        private val fullName: TextView = itemView.findViewById(R.id.full_name)
        private val roverName: TextView = itemView.findViewById(R.id.rover_name)
        private val imageView: ImageView = itemView.findViewById(R.id.mars_photo)

        @SuppressLint("RtlHardcoded")
        fun onBind(photoItem: PhotoInfo, onClick: (String?) -> Unit) {
            fullName.text = photoItem.fullName
            roverName.text = photoItem.roverName
            val margin = 5
            val radius = 5
            val transformation = RoundedCornersTransformation(50, 0)
            Picasso.get().load(photoItem.nasaImage).fit()
                .into(imageView)
            itemView.setOnClickListener {
                onClick(photoItem.nasaImage)
            }
        }
    }
}