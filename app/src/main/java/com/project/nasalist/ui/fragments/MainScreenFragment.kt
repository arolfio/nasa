package com.project.nasalist.ui.fragments


import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.project.nasalist.R
import com.project.nasalist.SampleApplication
import com.project.nasalist.contracts.NasaContract
import com.project.nasalist.databinding.FragmentMainScreenBinding
import com.project.nasalist.model.Photo
import com.project.nasalist.model.PhotoInfo
import com.project.nasalist.network.NasaService
import com.project.nasalist.presenters.MainPresenter
import com.project.nasalist.ui.adapters.NasaListAdapter
import java.lang.ref.WeakReference
import javax.inject.Inject


class MainScreenFragment : BaseFragment<FragmentMainScreenBinding>(), NasaContract.View,
    NasaListAdapter.NasaItemInterface {

    @Inject
    lateinit var api: NasaService

    private lateinit var presenter: MainPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        SampleApplication.INSTANCE.appComponent.injectMain(this)
        presenter = MainPresenter(api, this)
        super.onCreate(savedInstanceState)
    }

    override fun initBinding(
        inflater: LayoutInflater,
        container: ViewGroup?
    ): FragmentMainScreenBinding {
        return FragmentMainScreenBinding.inflate(inflater, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        presenter.start()
    }

    override fun init() {
        GridLayoutManager(
            activity,
            2,
            RecyclerView.VERTICAL,
            false
        ).apply {
            binding.recyclerNasaList.layoutManager = this
        }
        presenter.loadNasa()
    }

    override fun loadDataInList(data: List<Photo>?) {
        val list: MutableList<PhotoInfo> = mutableListOf()
        for (i in 0..(data?.size?.minus(1) ?: 11)) {
            data?.get(i)?.toPhotoInfo()?.let { list.add(it) }
        }
        val customAdapter = NasaListAdapter(WeakReference(this))
        customAdapter.setItems(list)
        Log.d("Result", "There are $list")

        binding.recyclerNasaList.adapter = customAdapter
    }

    private fun Photo.toPhotoInfo(): PhotoInfo = PhotoInfo(
        nasaImage = img_src,
        fullName = camera.full_name,
        roverName = rover.name
    )

    override fun onNasaItemClicked(image: String?) {

        val detailFragment = BigPictureFragment()
        val bundle = Bundle()
        bundle.putString("AA", image)
        detailFragment.arguments = bundle
        val transaction = activity?.supportFragmentManager?.beginTransaction()
        transaction?.replace(R.id.fragment_container, detailFragment)
        transaction?.disallowAddToBackStack()
        transaction?.commit()
    }
}