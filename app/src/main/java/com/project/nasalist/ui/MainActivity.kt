package com.project.nasalist.ui


import android.Manifest
import android.annotation.SuppressLint
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.content.pm.PackageManager
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.app.NotificationCompat
import androidx.core.content.ContextCompat
import com.github.terrakok.cicerone.*
import com.github.terrakok.cicerone.androidx.AppNavigator
import com.project.nasalist.R
import com.project.nasalist.SampleApplication
import com.project.nasalist.databinding.ActivityMainBinding
import com.project.nasalist.navigation.BackButtonListener
import com.project.nasalist.navigation.Screens
import com.project.nasalist.navigation.Screens.main
import com.project.nasalist.navigation.Screens.map
import javax.inject.Inject


private const val NOTIFICATION_INFO = "Nav"
class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding

    @Inject
    lateinit var router: Router

    @Inject
    lateinit var navigatorHolder: NavigatorHolder

    private val navigator: Navigator = object : AppNavigator(this, R.id.fragment_container) {

        override fun applyCommands(commands: Array<out Command>) {
            super.applyCommands(commands)
            supportFragmentManager.executePendingTransactions()
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        SampleApplication.INSTANCE.appComponent.injectMain(this)
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        val view = binding.root

        if (ContextCompat.checkSelfPermission(
                this,
                Manifest.permission.WRITE_EXTERNAL_STORAGE
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            ActivityCompat.requestPermissions(
                this,
                arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE), 1
            )
        }

        window.statusBarColor = ContextCompat.getColor(this, R.color.black)

        setContentView(view)

        binding.bottomNavigationMenu.itemIconTintList = null

        binding.bottomNavigationMenu.setOnItemSelectedListener {

            when (it.itemId) {
                R.id.menu_home -> router.navigateTo(Screens.main())
                R.id.menu_settings -> router.navigateTo(Screens.map())
            }
            true
        }

        val broadcastReceiverForCharging = object : BroadcastReceiver() {

            override fun onReceive(p0: Context?, p1: Intent?) {
                if (p1?.action == Intent.ACTION_POWER_CONNECTED) {
                    createNotification()
                }
            }
        }

        val intentFilterForCharging = IntentFilter()
        intentFilterForCharging.addAction(Intent.ACTION_POWER_CONNECTED)
        registerReceiver(broadcastReceiverForCharging, intentFilterForCharging)

        val infoFromNotification = intent.getStringExtra("MAP")

        if (savedInstanceState == null) {
            navigator.applyCommands(arrayOf<Command>(Replace(main())))
        }

        if (infoFromNotification == NOTIFICATION_INFO) {
            navigator.applyCommands(arrayOf<Command>(Replace(map())))
        }
    }

    override fun onResumeFragments() {
        super.onResumeFragments()
        navigatorHolder.setNavigator(navigator)
    }

    override fun onPause() {
        navigatorHolder.removeNavigator()
        super.onPause()
    }

    override fun onBackPressed() {
        val fragment = supportFragmentManager.findFragmentById(R.id.fragment_container)
        if (fragment != null && fragment is BackButtonListener
            && (fragment as BackButtonListener).onBackPressed()
        ) {
            return
        } else {
            super.onBackPressed()
        }
    }

    @SuppressLint("UnspecifiedImmutableFlag")
    private fun createNotification() {

        val intent = Intent(this, MainActivity::class.java)
        intent.putExtra("MAP", NOTIFICATION_INFO)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)

        val pendingIntent = PendingIntent.getActivity(
            this, 0, intent,
            PendingIntent.FLAG_ONE_SHOT
        )

        val notificationBuilder = NotificationCompat.Builder(this)
            .setSmallIcon(R.mipmap.ic_launcher)
            .setContentTitle(title)
            .setContentText("it’s time to explore our Earth!")
            .setAutoCancel(true)
            .addAction(R.drawable.map_icon, "Explore", pendingIntent)

        val notificationManager =
            getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        notificationManager.notify(0, notificationBuilder.build())
    }
}