package com.project.nasalist.ui

import android.content.Intent
import android.graphics.Color
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.view.View
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import com.google.android.material.snackbar.BaseTransientBottomBar
import com.google.android.material.snackbar.Snackbar
import com.project.nasalist.R


class SplashActivity : AppCompatActivity() {
    @RequiresApi(Build.VERSION_CODES.M)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        window.decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_FULLSCREEN
        actionBar?.hide()

        setContentView(R.layout.activity_splash)
        val intent = Intent(this, MainActivity::class.java)
        Handler().postDelayed(
            {
                if (isNetworkAvailable()) {
                    startActivity(intent)
                    finish()
                } else {
                    val view = findViewById<View>(android.R.id.content)
                    Snackbar.make(view, "Check your internet connection", Snackbar.LENGTH_LONG)
                        .setAnimationMode(BaseTransientBottomBar.ANIMATION_MODE_FADE)
                        .setBackgroundTint(Color.parseColor("#006400")).setAction("Ok") {
                        }.show()
                }
            }, 2000
        )
    }

    @RequiresApi(Build.VERSION_CODES.M)
    fun isNetworkAvailable(): Boolean {

        val cm = getSystemService(CONNECTIVITY_SERVICE) as ConnectivityManager
        val capabilities = cm.getNetworkCapabilities(cm.activeNetwork)

        return (capabilities != null && capabilities.hasCapability(NetworkCapabilities.NET_CAPABILITY_INTERNET))
    }
}