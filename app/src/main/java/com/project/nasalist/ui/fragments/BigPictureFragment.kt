package com.project.nasalist.ui.fragments


import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.graphics.drawable.BitmapDrawable
import android.net.Uri
import android.os.Bundle
import android.os.StrictMode
import android.provider.MediaStore
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.appcompat.app.AlertDialog
import com.project.nasalist.R
import com.project.nasalist.databinding.FragmentBigPictureBinding
import com.squareup.picasso.Picasso


private const val APP_PREFERENCES = "my_settings"

class BigPictureFragment : BaseFragment<FragmentBigPictureBinding>() {

    private lateinit var imageBut: ImageView
    private var mSettings: SharedPreferences? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val builder: StrictMode.VmPolicy.Builder = StrictMode.VmPolicy.Builder()
        StrictMode.setVmPolicy(builder.build())
    }

    override fun initBinding(
        inflater: LayoutInflater,
        container: ViewGroup?
    ): FragmentBigPictureBinding {
        return FragmentBigPictureBinding.inflate(inflater, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        var bundle = Bundle()
        bundle = this.requireArguments()
        val image = bundle.getString("AA")
        Picasso.get().load(image).fit().into(binding.detailImage)

        mSettings = activity?.getSharedPreferences(APP_PREFERENCES, Context.MODE_PRIVATE)

        binding.shareButton.setOnClickListener {

            val bitmapDrawable = binding.detailImage.drawable as BitmapDrawable
            val bitmap = bitmapDrawable.bitmap
            val bitmapPath = MediaStore.Images.Media.insertImage(
                requireActivity().contentResolver,
                bitmap,
                "Some title",
                null
            )
            val bitmapUri = Uri.parse(bitmapPath)
            val intent = Intent(Intent.ACTION_SEND)
            intent.type = "image/png"
            intent.putExtra(Intent.EXTRA_STREAM, bitmapUri)
            startActivity(Intent.createChooser(intent, "Share Image"))
        }

        binding.backButton.setOnClickListener {
            val transaction = activity?.supportFragmentManager?.beginTransaction()
            transaction?.replace(R.id.fragment_container, MainScreenFragment())
            transaction?.disallowAddToBackStack()
            transaction?.commit()
        }
    }

    @SuppressLint("ResourceType")
    override fun onStart() {
        super.onStart()
        if (mSettings?.getBoolean("first_run", true)!!) {
            val dialog =
                activity?.let {
                    AlertDialog.Builder(it, R.style.DialogTheme)
                        .setView(R.layout.alert_layoutt)
                        .setCancelable(true)
                        .show()
                }
            dialog?.window?.decorView?.setOnClickListener {
                dialog.dismiss()
            }
            imageBut = dialog?.findViewById(R.id.image_close)!!
            imageBut.setOnClickListener {
                dialog.dismiss()

            }
            mSettings!!.edit().putBoolean("first_run", false).apply()
        }
    }
}
