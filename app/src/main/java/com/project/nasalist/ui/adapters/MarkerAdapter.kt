package com.project.nasalist.ui.adapters

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.ImageButton
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.project.nasalist.R
import com.project.nasalist.model.Marker
import java.lang.ref.WeakReference

class MarkerAdapter(private val callbackWeakRef: WeakReference<MarkerItemInterface>) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private val markerItems = mutableListOf<Marker>()

    interface MarkerItemInterface {
        fun onMarkerItemClicked(index: Int?)

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return MarkerItemViewHolder(parent)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        (holder as MarkerItemViewHolder).onBind(markerItems[position], position, onClick = {
            callbackWeakRef.get()?.onMarkerItemClicked(it)
        })
    }

    override fun getItemCount(): Int {
        return markerItems.size
    }

    @SuppressLint("NotifyDataSetChanged")
    fun setItems(markerItems: List<Marker>) {
        this.markerItems.clear()
        this.markerItems.addAll(markerItems)
        notifyDataSetChanged()
    }

    @SuppressLint("NotifyDataSetChanged")
    fun deleteItem(index: Int) {
        this.markerItems.removeAt(index)
        notifyDataSetChanged()
    }

    inner class MarkerItemViewHolder(parent: ViewGroup) : RecyclerView.ViewHolder(
        LayoutInflater.from(parent.context).inflate(R.layout.marker_item, parent, false)
    ) {

        private val markerName: TextView = itemView.findViewById(R.id.marker_name)
        private val widthPoint: TextView = itemView.findViewById(R.id.width)
        private val longitudePoint: TextView = itemView.findViewById(R.id.longitude)
        private val deleteButton: ImageButton = itemView.findViewById(R.id.delete_button)

        fun onBind(markerItem: Marker, index: Int, onClick: (Int?) -> Unit) {
            if (index == 0) {
                itemView.setBackgroundResource(R.drawable.back)
            }
            markerName.text = markerItem.markerName
            longitudePoint.text = "Longitude: " + markerItem.markerLongitude
            widthPoint.text = "Width: " + markerItem.markerWidth

            deleteButton.setOnClickListener {
                deleteItem(index)
                onClick(index)
            }
        }
    }
}