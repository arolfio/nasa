package com.project.nasalist.ui.fragments

import android.graphics.Bitmap
import android.graphics.Canvas
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.PopupMenu
import androidx.appcompat.app.AlertDialog
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.BitmapDescriptor
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.android.gms.maps.model.MarkerOptions
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.project.nasalist.R
import com.project.nasalist.SampleApplication
import com.project.nasalist.databinding.FragmentMapBinding
import com.project.nasalist.model.Marker
import com.project.nasalist.ui.adapters.MarkerAdapter
import java.lang.ref.WeakReference


class MapFragment : BaseFragment<FragmentMapBinding>(), OnMapReadyCallback,
    MarkerAdapter.MarkerItemInterface {

    private lateinit var mMap: GoogleMap
    private lateinit var alertButtonCancel: Button
    private lateinit var alertButtonSave: Button
    private lateinit var markerName: EditText
    private lateinit var recyclerView: RecyclerView
    private lateinit var bottomSheetBehavior: BottomSheetBehavior<ConstraintLayout>

    private val customAdapter = MarkerAdapter(WeakReference(this))
    private val addMarkerList: MutableList<Marker> = mutableListOf()

    override fun onCreate(savedInstanceState: Bundle?) {
        SampleApplication.INSTANCE.appComponent.injectMap(this)
        super.onCreate(savedInstanceState)
    }

    override fun initBinding(
        inflater: LayoutInflater,
        container: ViewGroup?
    ): FragmentMapBinding {
        return FragmentMapBinding.inflate(inflater, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        recyclerView = view.findViewById(R.id.recycler_marker_list)

        val mapFragment = childFragmentManager
            .findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)

        bottomSheetBehavior = BottomSheetBehavior.from(view.findViewById(R.id.bottom_sheet))
        bottomSheetBehavior.state = BottomSheetBehavior.STATE_COLLAPSED
    }

    override fun onMapReady(googleMap: GoogleMap) {
        mMap = googleMap

        binding.showPopUp.setOnClickListener {
            val popupMenu = PopupMenu(activity, binding.showPopUp)
            popupMenu.setOnMenuItemClickListener {
                when (it.itemId) {
                    R.id.sort_by_hybrid -> mMap.mapType = GoogleMap.MAP_TYPE_HYBRID
                    R.id.sort_by_satellite -> mMap.mapType = GoogleMap.MAP_TYPE_SATELLITE
                    R.id.sort_by_terrain -> mMap.mapType = GoogleMap.MAP_TYPE_TERRAIN
                    R.id.sort_by_normal -> mMap.mapType = GoogleMap.MAP_TYPE_NORMAL
                }
                true
            }
            popupMenu.inflate(R.menu.sort_menu)
            popupMenu.show()
        }

        mMap.setOnMapClickListener { position ->

            val dialog =
                AlertDialog.Builder(requireContext()).setView(R.layout.custom_alert)
                    .setCancelable(true)
                    .show()
            alertButtonCancel = dialog.findViewById(R.id.alert_button_cancel)!!
            alertButtonSave = dialog.findViewById(R.id.alert_button_save)!!
            markerName = dialog.findViewById(R.id.marker_name)!!

            alertButtonSave.setOnClickListener {
                mMap.addMarker(MarkerOptions().position(position).title(markerName.text.toString()))
                    ?.setIcon(
                        bitMapFromVector(R.drawable.custom_marker)
                    )
                val marker =
                    Marker(
                        markerName.text.toString(),
                        position.longitude.toString(),
                        position.latitude.toString()
                    )
                addMarkerList.add(marker)
                recyclerView.adapter = customAdapter
                customAdapter.setItems(addMarkerList)
                dialog.cancel()
            }

            alertButtonCancel.setOnClickListener {
                dialog.cancel()
            }
            dialog.window?.setBackgroundDrawable(ColorDrawable(0))
        }
    }

    private fun bitMapFromVector(vectorResID: Int): BitmapDescriptor {
        val vectorDrawable = ContextCompat.getDrawable(requireContext(), vectorResID)
        vectorDrawable!!.setBounds(
            0,
            0,
            vectorDrawable.intrinsicWidth,
            vectorDrawable.intrinsicHeight
        )
        val bitmap =
            Bitmap.createBitmap(
                vectorDrawable.intrinsicWidth,
                vectorDrawable.intrinsicHeight,
                Bitmap.Config.ARGB_8888
            )
        val canvas = Canvas(bitmap)
        vectorDrawable.draw(canvas)
        return BitmapDescriptorFactory.fromBitmap(bitmap)
    }

    override fun onMarkerItemClicked(index: Int?) {
        if (index != null) {
            addMarkerList.removeAt(index)
        }
    }
}