package com.project.nasalist.navigation

interface BackButtonListener {

    fun onBackPressed(): Boolean

}