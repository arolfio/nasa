package com.project.nasalist.navigation

import com.github.terrakok.cicerone.androidx.FragmentScreen
import com.project.nasalist.ui.fragments.BigPictureFragment
import com.project.nasalist.ui.fragments.MainScreenFragment
import com.project.nasalist.ui.fragments.MapFragment


object Screens {

    fun main() = FragmentScreen { MainScreenFragment() }

    fun map() = FragmentScreen { MapFragment() }

    fun big() = FragmentScreen { BigPictureFragment() }
}