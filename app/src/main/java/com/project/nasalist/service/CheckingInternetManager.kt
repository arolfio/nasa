package com.project.nasalist.service

import android.app.Service
import android.content.Intent
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import android.os.Build
import android.os.IBinder
import androidx.annotation.RequiresApi
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

class CheckingInternetManager : Service() {
    override fun onBind(p0: Intent?): IBinder? {
        TODO("Not yet implemented")
    }


    @RequiresApi(Build.VERSION_CODES.M)
    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        GlobalScope.launch {
            sendInfoAboutInternet()
        }
        return START_NOT_STICKY
    }

    @RequiresApi(Build.VERSION_CODES.M)
    fun isNetworkAvailable(): Boolean {
        val cm = getSystemService(CONNECTIVITY_SERVICE) as ConnectivityManager
        val capabilities = cm.getNetworkCapabilities(cm.activeNetwork)

        return (capabilities != null && capabilities.hasCapability(NetworkCapabilities.NET_CAPABILITY_INTERNET))
    }


    @RequiresApi(Build.VERSION_CODES.M)
    suspend fun sendInfoAboutInternet() {
        val intent = Intent("Broadcast")

        while (true) {
            if (isNetworkAvailable()) {
                intent.putExtra("ASD", 1)
                sendBroadcast(intent)
            } else {
                intent.putExtra("ASD", 2)
                sendBroadcast(intent)
            }
            delay(5000)
        }
    }
}

