package com.project.nasalist.testrepo


import android.annotation.SuppressLint
import com.project.nasalist.callbacks.Callback
import com.project.nasalist.model.Photo
import com.project.nasalist.network.NasaService
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers.io


class TestRepository(private val apiClient: NasaService) {

    @SuppressLint("CheckResult")
    fun getNasaInfo(callback: Callback<List<Photo>?>) {
        apiClient.getNasaList()
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(io())
            .subscribe({ result ->
                callback.returnResult(result.photos)
            }, { error ->
                error.printStackTrace()
            })
    }
}

