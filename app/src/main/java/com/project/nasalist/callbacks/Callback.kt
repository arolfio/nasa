package com.project.nasalist.callbacks

import com.project.nasalist.model.Photo

abstract class Callback<T> {

    abstract fun returnResult(t: List<Photo>?)
    abstract fun returnError(message: String)
}