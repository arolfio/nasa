package com.project.nasalist.network

import com.project.nasalist.model.GetNasaListResponse
import io.reactivex.Observable
import retrofit2.http.GET


interface NasaService {

    @GET("rovers/curiosity/photos?sol=1000&api_key=PfoYutQC0VioYidYOmT9yIqLilOytleLpCXSeMov")
    fun getNasaList(): Observable<GetNasaListResponse>

}