package com.project.nasalist

import android.app.Application
import com.project.nasalist.di.AppComponent
import com.project.nasalist.di.DaggerAppComponent


class SampleApplication : Application() {

    val appComponent: AppComponent by lazy {
        DaggerAppComponent.builder().build()
    }

    override fun onCreate() {
        super.onCreate()
        INSTANCE = this
    }

    companion object {
        lateinit var INSTANCE: SampleApplication
    }
}