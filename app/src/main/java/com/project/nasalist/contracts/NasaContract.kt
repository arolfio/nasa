package com.project.nasalist.contracts

import com.project.nasalist.model.Photo

interface NasaContract {

    interface View {

        fun init()

        fun loadDataInList(data: List<Photo>?)
    }

    interface Presenter {

        fun start()

        fun loadNasa()
    }
}