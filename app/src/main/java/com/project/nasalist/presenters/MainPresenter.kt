package com.project.nasalist.presenters

import com.project.nasalist.callbacks.Callback
import com.project.nasalist.contracts.NasaContract
import com.project.nasalist.model.Photo
import com.project.nasalist.network.NasaService
import com.project.nasalist.testrepo.TestRepository


class MainPresenter(private val api: NasaService, val mView: NasaContract.View) :
    NasaContract.Presenter {

    override fun start() {
        mView.init()
    }

    override fun loadNasa() {

        TestRepository(api).getNasaInfo(object : Callback<List<Photo>?>() {
            override fun returnResult(t: List<Photo>?) {
                if (t != null) {
                    mView.loadDataInList(t)
                }
            }

            override fun returnError(message: String) {
            }
        })
    }
}
