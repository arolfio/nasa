package com.project.nasalist.di

import com.project.nasalist.di.modules.NavigationModule
import com.project.nasalist.di.modules.NetworkModule
import com.project.nasalist.ui.MainActivity

import com.project.nasalist.ui.fragments.MainScreenFragment
import com.project.nasalist.ui.fragments.MapFragment

import javax.inject.Singleton


@dagger.Component(
    modules = [
        NavigationModule::class,
        NetworkModule::class]
)

@Singleton
interface AppComponent {

    fun injectMain(activity: MainActivity)

    fun injectMain(fragment: MainScreenFragment)

    fun injectMap(fragment: MapFragment)
}