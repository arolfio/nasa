package com.project.nasalist.model

data class PhotoInfo(
    val nasaImage: String,
    val fullName: String,
    val roverName: String
)