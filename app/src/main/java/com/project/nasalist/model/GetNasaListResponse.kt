package com.project.nasalist.model

data class GetNasaListResponse(
    val photos: List<Photo>?
)