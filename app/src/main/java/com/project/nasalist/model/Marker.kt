package com.project.nasalist.model

data class Marker(
    val markerName: String?,
    val markerLongitude: String?,
    val markerWidth: String?
)